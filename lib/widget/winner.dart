import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Winner extends StatelessWidget {
  Winner({this.position});
  int position;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2, vertical: 8),
      height: position == 1 ? 300 : 250,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
              bottom: 0,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Image.asset(
                    'assets/cube.png',
                    width: position == 1
                        ? MediaQuery.of(context).size.width * .32
                        : MediaQuery.of(context).size.width * .28,
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 18.0),
                    child: Text(
                      '#$position',
                      style: GoogleFonts.cairo().copyWith(
                          color: Colors.blue.shade900,
                          fontSize: position == 1 ? 24 : 20,
                          fontWeight: FontWeight.w900),
                    ),
                  ),
                ],
              )),
          Positioned(
              top: 0,
              bottom: 60,
              child: Image.asset(
                'assets/winner.png',
              )),
        ],
      ),
    );
  }
}
