import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Contact extends StatelessWidget {
  int i;
  bool isSearch;
  Contact({this.i, this.isSearch = false});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 4.0),
      height: 110,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Transform(
              transform: Matrix4.skewX(-0.2),
              child: Container(
                padding: EdgeInsets.only(right: 50),
                alignment: Alignment.centerRight,
                margin: EdgeInsets.symmetric(vertical: 4),
                color: i != 3 ? Color(0xFFfdba2c) : Color(0xFF1d398d),
                height: 100,
                width: MediaQuery.of(context).size.width * .8,
                child: Wrap(
                  direction: Axis.vertical,
                  crossAxisAlignment: WrapCrossAlignment.end,
                  children: [
                    Text(
                      'User',
                      style: GoogleFonts.cairo().copyWith(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w700),
                    ),
                    Visibility(
                        visible: isSearch,
                        child: Text('00:35.32',
                            style: GoogleFonts.cairo().copyWith(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w900)))
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: EdgeInsets.only(left: 10),
              child: Transform(
                transform: Matrix4.skewX(-0.2),
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    height: 60,
                    width: 70,
                    padding: EdgeInsets.only(right: 4),
                    color: Color(0xFF777777),
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Visibility(
            visible: isSearch,
            child: Positioned(
              right: 15,
              top: 25,
              height: 60,
              child: Container(
                child:
                    // Transform(
                    //   transform: Matrix4.skewX(-0.2),  // needs to be uncommented when in dev
                    //   child:
                    Container(
                  // decoration: BoxDecoration(
                  //   border: Border.all(color: Colors.white, width: 3.5), // needs to be uncommented when in dev
                  // ),
                  child: Center(
                      child: Image.asset(
                    'assets/pp.png',
                    fit: BoxFit.cover,
                    height: 60,
                  )),
                ),
                // ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
