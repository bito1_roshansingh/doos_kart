import 'package:flutter/material.dart';

class FClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.moveTo(10, 0);

    path.lineTo(size.width, 0);
    path.lineTo(size.width - 10, size.height);
    path.lineTo(0, size.height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(FClipper oldClipper) => false;
}
