import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Participants extends StatelessWidget {
  int i;
  Participants(this.i);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: ClipPath(
              clipper: SkewCut(),
              child: Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.symmetric(vertical: 8),
                color: i == 1
                    ? Color(0xFF1d398d)
                    : i > 2
                        ? Colors.grey
                        : Color(0xFFfdba2c),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Opacity(
                      opacity: 0,
                      child: Row(
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                'Gap',
                                textAlign: TextAlign.right,
                                style: GoogleFonts.cairo().copyWith(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w300),
                              ),
                              Text(
                                '+01.464',
                                textAlign: TextAlign.right,
                                style: GoogleFonts.cairo().copyWith(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Lab',
                                textAlign: TextAlign.right,
                                style: GoogleFonts.cairo().copyWith(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w300),
                              ),
                              Text(
                                '5',
                                textAlign: TextAlign.right,
                                style: GoogleFonts.cairo().copyWith(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Text(
                              'Gap',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300),
                            ),
                            Text(
                              '+01.464',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              'Lab',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300),
                            ),
                            Text(
                              '5',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              'User',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.w700),
                            ),
                            Text(
                              '00:35.32',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          alignment: Alignment.center,
                          child: Image.asset('assets/d.png'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            left: 0,
            bottom: 10,
            top: 10,
            child: Container(
              child: Transform(
                transform: Matrix4.skewX(-0.12),
                child: Container(
                  color: Color(0xFF777777),
                  width: 70,
                  child: Center(
                      child: Text(
                    '#${i + 1}',
                    style: GoogleFonts.cairo().copyWith(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.w700),
                  )),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SkewCut extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.moveTo(20, 0);

    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(SkewCut oldClipper) => false;
}
