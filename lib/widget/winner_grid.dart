import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'friend_button_clipper.dart';

class WinnerGrid extends StatelessWidget {
  int i;
  WinnerGrid(this.i);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: i > 2 ? EdgeInsets.all(5) : EdgeInsets.zero,
      child: Stack(
        children: [
          ClipPath(
            clipper: FClipper(),
            child: Container(
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.only(
                top: 14,
              ),
              child: Column(children: [
                Container(
                  width: MediaQuery.of(context).size.width * .3,
                  color: i == 2 ? Colors.amber : Colors.white,
                  padding: EdgeInsets.only(right: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: i == 1
                            ? 55
                            : i < 3
                                ? 40
                                : 35,
                      ),
                      Text(
                        'User',
                        style: GoogleFonts.cairo(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.blue.shade900),
                      ),
                      Text(
                        '00:35.32',
                        style: GoogleFonts.cairo(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.blue.shade900),
                      ),
                      SizedBox(
                        height: i == 1
                            ? 0
                            : i < 3
                                ? 10
                                : 5,
                      )
                    ],
                  ),
                ),
              ]),
            ),
          ),
          Positioned(
              bottom: 0,
              left: 0,
              child: ClipPath(
                clipper: FClipper(),
                child: Container(
                  color: Colors.yellow,
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                  child: Text(
                    i == 0
                        ? '#2'
                        : i == 1
                            ? '#1'
                            : i == 2
                                ? '#3'
                                : '#${i + 1}',
                    style: GoogleFonts.cairo(
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                        color: Colors.blue.shade900),
                  ),
                ),
              )),
          Positioned(
              top: i == 1 ? 0 : 5,
              right: 8,
              child: Image.asset(
                'assets/d.png',
                height: i == 1
                    ? 56
                    : i < 3
                        ? 40
                        : 35,
              )),
        ],
      ),
    );
  }
}
