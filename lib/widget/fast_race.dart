import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FastRace extends StatelessWidget {
  int i;
  FastRace(this.i);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 4.0),
      height: 110,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Transform(
              transform: Matrix4.skewX(-0.1),
              child: Container(
                padding: EdgeInsets.only(right: 50),
                alignment: Alignment.centerRight,
                margin: EdgeInsets.symmetric(vertical: 4),
                color: i < 3
                    ? Color(0xFFfdba2c)
                    : i == 4
                        ? Color(0xFF1d398d)
                        : Color(0xFF959595),
                height: 100,
                width: MediaQuery.of(context).size.width * .8,
                child: Wrap(
                  direction: Axis.vertical,
                  crossAxisAlignment: WrapCrossAlignment.end,
                  children: [
                    Text(
                      'User',
                      style: GoogleFonts.cairo().copyWith(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w700),
                    ),
                    Text('00:35:32',
                        style: GoogleFonts.cairo().copyWith(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w900))
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: EdgeInsets.only(left: 10),
              child: Transform(
                transform: Matrix4.skewX(-0.12),
                child: Container(
                  color: Color(0xFF777777),
                  height: 60,
                  width: 70,
                  child: Center(
                      child: Text(
                    '#${i + 1}',
                    style: GoogleFonts.cairo().copyWith(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.w700),
                  )),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Transform(
              transform: Matrix4.skewX(-0.12),
              child: Container(
                  height: 60,
                  width: 70,
                  child: Center(
                    child: Image.asset('assets/d.png'),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
