import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Friends extends StatelessWidget {
  int i;
  Friends(this.i);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: ClipPath(
              clipper: SkewCut(),
              child: Container(
                padding: EdgeInsets.only(right: 20),
                alignment: Alignment.centerRight,
                margin: EdgeInsets.symmetric(vertical: 4),
                color: i == 1 ? Color(0xFF1d398d) : Color(0xFFfdba2c),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          'Sultan\nAlshareef',
                          textAlign: TextAlign.right,
                          style: GoogleFonts.cairo().copyWith(
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.w700),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          alignment: Alignment.center,
                          child: Image.asset('assets/d.png'),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      children: [
                        Column(
                          children: [
                            Image.asset('assets/go_kart.png'),
                            Text(
                              '#12',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                            Opacity(
                              opacity: .59,
                              child: Text(
                                'Races No.',
                                textAlign: TextAlign.right,
                                style: GoogleFonts.cairo().copyWith(
                                    color: Colors.white,
                                    fontSize: 10,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Image.asset('assets/speed.png'),
                            Text(
                              '10.5 km',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                            Opacity(
                              opacity: .59,
                              child: Text(
                                'Total km',
                                textAlign: TextAlign.right,
                                style: GoogleFonts.cairo().copyWith(
                                    color: Colors.white,
                                    fontSize: 10,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Image.asset('assets/time.png'),
                            Text(
                              '00:06',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                            Opacity(
                              opacity: .59,
                              child: Text(
                                'Total Time',
                                textAlign: TextAlign.right,
                                style: GoogleFonts.cairo().copyWith(
                                    color: Colors.white,
                                    fontSize: 10,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Image.asset('assets/journey.png'),
                            Text(
                              '30:32.323',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                            Opacity(
                              opacity: .59,
                              child: Text(
                                'Fast lap',
                                textAlign: TextAlign.right,
                                style: GoogleFonts.cairo().copyWith(
                                    color: Colors.white,
                                    fontSize: 10,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            left: 0,
            height: 60,
            top: 20,
            child: Container(
              margin: EdgeInsets.only(left: 10),
              child: Transform(
                transform: Matrix4.skewX(-0.12),
                child: Container(
                  color: Color(0xFF777777),
                  width: 70,
                  child: Center(
                      child: Text(
                    '#${i + 1}',
                    style: GoogleFonts.cairo().copyWith(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.w700),
                  )),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SkewCut extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.moveTo(20, 0);

    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(SkewCut oldClipper) => false;
}
