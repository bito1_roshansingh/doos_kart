import 'package:doos_kart/widget/friend_button_clipper.dart';
import 'package:doos_kart/widget/participants.dart';
import 'package:doos_kart/widget/winner_grid.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ShareScorePage extends StatefulWidget {
  @override
  _ShareScorePageState createState() => _ShareScorePageState();
}

class _ShareScorePageState extends State<ShareScorePage>
    with TickerProviderStateMixin {
  int currentTab = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.amber,
          padding: EdgeInsets.symmetric(horizontal: 8),
          alignment: Alignment.bottomCenter,
          child: ClipPath(
            clipper: ScoreCut(),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
              height: MediaQuery.of(context).size.height * .7,
              color: Colors.blue.shade900,
              child: Column(
                children: [
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 4,
                          fit: FlexFit.tight,
                          child: Row(
                            children: [
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 1,
                                child: Transform(
                                  transform: Matrix4.skewX(-0.2),
                                  child: InkWell(
                                    onTap: () {},
                                    child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.white, width: 2)),
                                        child: Material(
                                            color: currentTab != 0
                                                ? Colors.white
                                                : Colors.blue.shade900,
                                            child: InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    currentTab = 0;
                                                  });
                                                },
                                                child: Container(
                                                    alignment: Alignment.center,
                                                    child: Icon(
                                                      Icons.check,
                                                      color: currentTab == 0
                                                          ? Colors.white
                                                          : Colors
                                                              .blue.shade900,
                                                    ))))),
                                  ),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 1,
                                child: Transform(
                                  transform: Matrix4.skewX(-0.2),
                                  child: InkWell(
                                    onTap: () {},
                                    child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.white, width: 2)),
                                        child: Material(
                                            color: currentTab != 1
                                                ? Colors.white
                                                : Colors.blue.shade900,
                                            child: InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    currentTab = 1;
                                                  });
                                                },
                                                child: Container(
                                                    alignment: Alignment.center,
                                                    child: Icon(
                                                      Icons.check,
                                                      color: currentTab == 1
                                                          ? Colors.white
                                                          : Colors
                                                              .blue.shade900,
                                                    ))))),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Flexible(
                          flex: 6,
                          fit: FlexFit.tight,
                          child: Text(
                            'Share The Scoure',
                            textAlign: TextAlign.end,
                            style: GoogleFonts.cairo(
                                fontSize: 20,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Image.asset(
                              'assets/flag.png',
                            ))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Visibility(
                    visible: currentTab == 0,
                    child: Flexible(
                        fit: FlexFit.tight,
                        flex: 9,
                        child: Container(
                          decoration: BoxDecoration(
                              border:
                                  Border.all(color: Colors.white, width: 4)),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Opacity(
                                          opacity: 0,
                                          child: Image.asset(
                                              'assets/dooskarting.png')),
                                      Text(
                                        'نتائج السباق',
                                        style: GoogleFonts.cairo(
                                            fontSize: 20,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Image.asset('assets/dooskarting.png')
                                    ]),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height:
                                      MediaQuery.of(context).size.height * .3,
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 4, vertical: 8),
                                  child: GridView.builder(
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            childAspectRatio: 1,
                                            mainAxisSpacing: 10,
                                            crossAxisCount: 3),
                                    itemBuilder: (context, i) {
                                      return WinnerGrid(i);
                                    },
                                    itemCount: 6,
                                  ),
                                ),
                                Container(
                                  height: 50,
                                  margin: EdgeInsets.symmetric(horizontal: 24),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        fit: FlexFit.tight,
                                        flex: 2,
                                        child: Transform(
                                          transform: Matrix4.skewX(-0.25),
                                          child: InkWell(
                                            onTap: () {},
                                            child: Container(
                                                height: 50,
                                                padding:
                                                    EdgeInsets.only(right: 4),
                                                color: Color(0xFF959595),
                                                child: Image.asset(
                                                    'assets/upload.png')),
                                          ),
                                        ),
                                      ),
                                      Flexible(
                                        fit: FlexFit.tight,
                                        flex: 2,
                                        child: Transform(
                                          transform: Matrix4.skewX(-0.25),
                                          child: InkWell(
                                            onTap: () {},
                                            child: Container(
                                              height: 50,
                                              padding:
                                                  EdgeInsets.only(right: 4),
                                              color: Color(0xFF488d1d),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Flexible(
                                        fit: FlexFit.tight,
                                        flex: 2,
                                        child: Transform(
                                          transform: Matrix4.skewX(-0.25),
                                          child: InkWell(
                                            onTap: () {},
                                            child: Container(
                                              height: 50,
                                              padding:
                                                  EdgeInsets.only(right: 4),
                                              color: Color(0xFF4c79fd),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Flexible(
                                        fit: FlexFit.tight,
                                        flex: 2,
                                        child: Transform(
                                          transform: Matrix4.skewX(-0.25),
                                          child: InkWell(
                                            onTap: () {},
                                            child: Container(
                                              height: 50,
                                              padding:
                                                  EdgeInsets.only(right: 4),
                                              color: Color(0xFF5d20d0),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Flexible(
                                        fit: FlexFit.tight,
                                        flex: 2,
                                        child: Transform(
                                          transform: Matrix4.skewX(-0.25),
                                          child: InkWell(
                                            onTap: () {},
                                            child: Container(
                                              height: 50,
                                              padding:
                                                  EdgeInsets.only(right: 4),
                                              color: Color(0xFFfdba2c),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  color: Colors.transparent,
                                  margin: const EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 100),
                                  child: ClipPath(
                                      clipper: FClipper(),
                                      child: Material(
                                          color: Color(0xFF12a23c),
                                          child: InkWell(
                                              onTap: () {},
                                              child: Container(
                                                  height: 48,
                                                  width: 200,
                                                  alignment: Alignment.center,
                                                  child: Icon(
                                                    Icons.check,
                                                    color: Colors.white,
                                                  ))))),
                                )
                              ]),
                        )),
                  ),
                  Visibility(
                      visible: currentTab == 1,
                      child: Flexible(
                          fit: FlexFit.tight,
                          flex: 10,
                          child: Column(
                            children: [
                              Flexible(
                                flex: 1,
                                child: Stack(
                                  children: [
                                    ShaderMask(
                                      shaderCallback: (rect) {
                                        return LinearGradient(
                                          begin: Alignment.centerRight,
                                          end: Alignment.centerLeft,
                                          colors: [
                                            Colors.blue.shade900,
                                            Colors.transparent
                                          ],
                                        ).createShader(Rect.fromLTRB(
                                            0,
                                            0,
                                            rect.height + 150,
                                            rect.height - 50));
                                      },
                                      blendMode: BlendMode.darken,
                                      child: Container(
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    'assets/track.png'),
                                                fit: BoxFit.cover),
                                            border: Border.all(
                                                color: Colors.white, width: 4)),
                                      ),
                                    ),
                                    Container(
                                        alignment: Alignment.topRight,
                                        padding: EdgeInsets.only(right: 8),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.white, width: 4)),
                                        child: Stack(
                                          alignment: Alignment.topRight,
                                          children: [
                                            Positioned(
                                              top: 0,
                                              child: Text(
                                                'SULTAN',
                                                style: GoogleFonts.cairo(
                                                    fontSize: 47,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ),
                                            ),
                                            Positioned(
                                              top: 57,
                                              child: Text(
                                                'Best Lab',
                                                style: GoogleFonts.cairo(
                                                    fontSize: 28,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ),
                                            ),
                                            Positioned(
                                              top: 95,
                                              child: Text(
                                                '00:35.32',
                                                style: GoogleFonts.cairo(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.white),
                                              ),
                                            ),
                                            Positioned(
                                              top: 115,
                                              child: Text(
                                                'Lab',
                                                style: GoogleFonts.cairo(
                                                    fontSize: 28,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ),
                                            ),
                                            Positioned(
                                              top: 150,
                                              child: Text(
                                                '5',
                                                style: GoogleFonts.cairo(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Column(
                                    children: [
                                      Container(
                                        width: 100,
                                        margin: EdgeInsets.all(8),
                                        child: Row(
                                          children: [
                                            Flexible(
                                              fit: FlexFit.tight,
                                              flex: 1,
                                              child: Transform(
                                                transform: Matrix4.skewX(-0.2),
                                                child: InkWell(
                                                  onTap: () {},
                                                  child: Container(
                                                      decoration: BoxDecoration(
                                                          border: Border.all(
                                                              color:
                                                                  Colors.white,
                                                              width: 2)),
                                                      child: Material(
                                                          color: currentTab != 0
                                                              ? Colors.white
                                                              : Colors.blue
                                                                  .shade900,
                                                          child: InkWell(
                                                              onTap: () {},
                                                              child: Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child: Icon(
                                                                    Icons.check,
                                                                    color: currentTab ==
                                                                            0
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .blue
                                                                            .shade900,
                                                                  ))))),
                                                ),
                                              ),
                                            ),
                                            Flexible(
                                              fit: FlexFit.tight,
                                              flex: 1,
                                              child: Transform(
                                                transform: Matrix4.skewX(-0.2),
                                                child: InkWell(
                                                  onTap: () {},
                                                  child: Container(
                                                      decoration: BoxDecoration(
                                                          border: Border.all(
                                                              color:
                                                                  Colors.white,
                                                              width: 2)),
                                                      child: Material(
                                                          color: currentTab != 1
                                                              ? Colors.white
                                                              : Colors.blue
                                                                  .shade900,
                                                          child: InkWell(
                                                              onTap: () {},
                                                              child: Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child: Icon(
                                                                    Icons.check,
                                                                    color: currentTab ==
                                                                            1
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .blue
                                                                            .shade900,
                                                                  ))))),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        height: 50,
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 8, vertical: 8),
                                        child: Row(
                                          children: [
                                            Flexible(
                                              fit: FlexFit.tight,
                                              flex: 2,
                                              child: Transform(
                                                transform: Matrix4.skewX(-0.25),
                                                child: InkWell(
                                                  onTap: () {},
                                                  child: Container(
                                                      height: 50,
                                                      padding: EdgeInsets.only(
                                                          right: 4),
                                                      color: Color(0xFF959595),
                                                      child: Image.asset(
                                                          'assets/upload.png')),
                                                ),
                                              ),
                                            ),
                                            Flexible(
                                              fit: FlexFit.tight,
                                              flex: 2,
                                              child: Transform(
                                                transform: Matrix4.skewX(-0.25),
                                                child: InkWell(
                                                  onTap: () {},
                                                  child: Container(
                                                    height: 50,
                                                    padding: EdgeInsets.only(
                                                        right: 4),
                                                    color: Color(0xFF488d1d),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Flexible(
                                              fit: FlexFit.tight,
                                              flex: 2,
                                              child: Transform(
                                                transform: Matrix4.skewX(-0.25),
                                                child: InkWell(
                                                  onTap: () {},
                                                  child: Container(
                                                    height: 50,
                                                    padding: EdgeInsets.only(
                                                        right: 4),
                                                    color: Color(0xFF4c79fd),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Flexible(
                                              fit: FlexFit.tight,
                                              flex: 2,
                                              child: Transform(
                                                transform: Matrix4.skewX(-0.25),
                                                child: InkWell(
                                                  onTap: () {},
                                                  child: Container(
                                                    height: 50,
                                                    padding: EdgeInsets.only(
                                                        right: 4),
                                                    color: Color(0xFF5d20d0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Flexible(
                                              fit: FlexFit.tight,
                                              flex: 2,
                                              child: Transform(
                                                transform: Matrix4.skewX(-0.25),
                                                child: InkWell(
                                                  onTap: () {},
                                                  child: Container(
                                                    height: 50,
                                                    padding: EdgeInsets.only(
                                                        right: 4),
                                                    color: Color(0xFFfdba2c),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        color: Colors.transparent,
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 100),
                                        child: ClipPath(
                                            clipper: FClipper(),
                                            child: Material(
                                                color: Color(0xFF12a23c),
                                                child: InkWell(
                                                    onTap: () {},
                                                    child: Container(
                                                        height: 48,
                                                        width: 200,
                                                        alignment:
                                                            Alignment.center,
                                                        child: Icon(
                                                          Icons.check,
                                                          color: Colors.white,
                                                        ))))),
                                      )
                                    ],
                                  ))
                            ],
                          ))),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ScoreCut extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.moveTo(0, size.height * 0.02);

    path.lineTo(size.width, size.height * 0.005);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(ScoreCut oldClipper) => false;
}
