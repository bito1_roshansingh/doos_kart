import 'package:doos_kart/widget/contact.dart';
import 'package:doos_kart/widget/most_race.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AddFriendPage extends StatefulWidget {
  @override
  _AddFriendPageState createState() => _AddFriendPageState();
}

class _AddFriendPageState extends State<AddFriendPage> {
  TextEditingController _searchController = TextEditingController();
  bool isSearch = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: ClipPath(
          clipper: AppBarClipper(),
          child: Container(
            height: 110,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.chevron_left,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Text(
                  'Add Friends',
                  style: GoogleFonts.cairo()
                      .copyWith(fontSize: 24, color: Colors.white),
                ),
                Visibility(
                  //to center align row
                  visible: false,
                  child: IconButton(
                    icon: Icon(
                      Icons.chevron_left,
                      color: Colors.white,
                    ),
                    onPressed: () {},
                  ),
                ),
                Visibility(
                  //to center align row
                  visible: false,
                  child: IconButton(
                    icon: Icon(
                      Icons.chevron_left,
                      color: Colors.white,
                    ),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                  Color(0xFF1D3A8D),
                  Color(0xFF111C45),
                ])),
          ),
        ),
        preferredSize: Size.fromHeight(200),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                'Search',
                style: GoogleFonts.cairo().copyWith(
                    fontSize: 16,
                    color: Color(0xFF1D398D),
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.right,
              ),
            ),
            Container(
                height: 50,
                margin: EdgeInsets.only(right: 8, left: 24),
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 2,
                      child: Transform(
                        transform: Matrix4.skewX(-0.3),
                        child: InkWell(
                          onTap: () {
                            setState(() {});
                          },
                          child: Container(
                            height: 50,
                            padding: EdgeInsets.only(right: 4),
                            color: Color(0xFF1D398D),
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 8,
                      child: Transform(
                        transform: Matrix4.skewX(-0.3),
                        child: Container(
                          margin: EdgeInsets.only(left: 4),
                          child: TextFormField(
                            controller: _searchController,
                            decoration: InputDecoration(
                                labelText: 'Search',
                                labelStyle: GoogleFonts.cairo().copyWith(
                                    fontSize: 16, color: Color(0xFF1D398D)),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color(0xFF1D398D), width: 1),
                                    borderRadius: BorderRadius.zero),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color(0xFF1D398D), width: 1),
                                    borderRadius: BorderRadius.zero)),
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                'Share Invitation',
                style: GoogleFonts.cairo().copyWith(
                    fontSize: 16,
                    color: Color(0xFF1D398D),
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.right,
              ),
            ),
            Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: 24),
              child: Row(
                children: [
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 2,
                    child: Transform(
                      transform: Matrix4.skewX(-0.25),
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                            height: 50,
                            padding: EdgeInsets.only(right: 4),
                            color: Color(0xFF959595),
                            child: Image.asset('assets/upload.png')),
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 2,
                    child: Transform(
                      transform: Matrix4.skewX(-0.25),
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                            height: 50,
                            padding: EdgeInsets.only(right: 4),
                            color: Color(0xFF488d1d),
                            child: Image.asset('assets/whatsapp.png')),
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 2,
                    child: Transform(
                      transform: Matrix4.skewX(-0.25),
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                            height: 50,
                            padding: EdgeInsets.only(right: 4),
                            color: Color(0xFF4c79fd),
                            child: Image.asset('assets/twitter.png')),
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 2,
                    child: Transform(
                      transform: Matrix4.skewX(-0.25),
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                            height: 50,
                            padding: EdgeInsets.only(right: 4),
                            color: Color(0xFF5d20d0),
                            child: Image.asset('assets/instagram.png')),
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 2,
                    child: Transform(
                      transform: Matrix4.skewX(-0.25),
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                            height: 50,
                            padding: EdgeInsets.only(right: 4),
                            color: Color(0xFFfdba2c),
                            child: Image.asset('assets/snapchat.png')),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                'My Contact',
                style: GoogleFonts.cairo().copyWith(
                    fontSize: 16,
                    color: Color(0xFF1D398D),
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.right,
              ),
            ),
            Container(
              height: 1000,
              child: ListView.builder(
                itemBuilder: (context, i) => Contact(
                  i: i,
                  isSearch: _searchController.text.length > 2,
                ),
                itemCount: 5,
                primary: false,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class AppBarClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();

    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height - 10);
    path.lineTo(0, size.height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(AppBarClipper oldClipper) => false;
}

class SkewCut extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.moveTo(20, 0);

    path.lineTo(size.width + 20, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(SkewCut oldClipper) => false;
}
