import 'package:doos_kart/pages/friends_list.dart';
import 'package:doos_kart/pages/most_race_list.dart';
import 'package:doos_kart/widget/fast_race.dart';
import 'package:doos_kart/widget/friend_button_clipper.dart';
import 'package:doos_kart/widget/friends.dart';
import 'package:doos_kart/widget/most_race.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import 'fast_race_list.dart';

class RankPage extends StatefulWidget {
  @override
  _RankPageState createState() => _RankPageState();
}

class _RankPageState extends State<RankPage> with TickerProviderStateMixin {
  int currentTab = 2;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

    return Builder(builder: (context) {
      var _tabController =
          TabController(length: 3, vsync: this, initialIndex: currentTab);
      var _pageController = PageController(initialPage: currentTab);
      return Scaffold(
        appBar: PreferredSize(
          child: appBar(_tabController, _pageController),
          preferredSize: Size.fromHeight(200),
        ),
        body: PageView(
          onPageChanged: (_) {
            setState(() {
              currentTab = _;
            });
          },
          controller: _pageController,
          children: [FriendsList(), MostRaceList(), FastRaceList()],
        ),
      );
    });
  }

  appBar(TabController _tabController, PageController _pageController) {
    return Stack(
      children: [
        Column(
          children: [
            PhysicalModel(
              color: Colors.transparent,
              shadowColor: Colors.black,
              elevation: 3,
              child: Container(
                height: 200,
                width: double.maxFinite,
                child: Center(
                  child: Image.asset('assets/logo.png'),
                ),
                decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 5.0, color: Color(0xFFFDBA2C)),
                    ),
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Color(0xFF1B378C),
                          Color(0xFF7D2255),
                          Color(0xFFE43A0B),
                        ])),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
        Positioned(
          bottom: 0,
          left: 10,
          right: 10,
          child: Transform(
            transform: Matrix4.skewX(-0.1),
            child: Material(
              elevation: 3,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
                child: TabBar(
                  onTap: (i) {
                    setState(() {
                      currentTab = i;
                      _pageController.animateToPage(currentTab,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.easeInOut);
                    });
                  },
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorColor: Colors.transparent,
                  controller: _tabController,
                  unselectedLabelColor: Color(0xFF1d398d),
                  labelColor: Colors.white,
                  tabs: [
                    Transform(
                      transform: Matrix4.skewX(-0.2),
                      child: Tab(
                          icon: Container(
                        color:
                            currentTab == 0 ? Color(0xFF1d398d) : Colors.white,
                        child: Column(
                          children: [
                            Container(
                              width: double.maxFinite,
                              child: Icon(
                                Icons.ac_unit_outlined,
                                size: 20,
                              ),
                            ),
                            Text('Friends',
                                style: GoogleFonts.cairo().copyWith(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                    color: currentTab == 0
                                        ? Colors.white
                                        : Color(0xFF1d398d)))
                          ],
                        ),
                      )),
                    ),
                    Transform(
                      transform: Matrix4.skewX(-0.1),
                      child: Tab(
                          icon: Container(
                        color:
                            currentTab == 1 ? Color(0xFF1d398d) : Colors.white,
                        child: Column(
                          children: [
                            Container(
                              width: double.maxFinite,
                              child: Image.asset(
                                'assets/flag.png',
                                height: 20,
                              ),
                            ),
                            Text(
                              'Most Race',
                              style: GoogleFonts.cairo().copyWith(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  color: currentTab == 1
                                      ? Colors.white
                                      : Color(0xFF1d398d)),
                            )
                          ],
                        ),
                      )),
                    ),
                    Transform(
                      transform: Matrix4.skewX(-0.1),
                      child: Tab(
                          child: Container(
                        color:
                            currentTab == 2 ? Color(0xFF1d398d) : Colors.white,
                        child: Column(
                          children: [
                            Container(
                              width: double.maxFinite,
                              child: Image.asset(
                                'assets/fastest.png',
                                height: 20,
                              ),
                            ),
                            Text('Fast Rounds',
                                style: GoogleFonts.cairo().copyWith(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                    color: currentTab == 2
                                        ? Colors.white
                                        : Color(0xFF1d398d)))
                          ],
                        ),
                      )),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
