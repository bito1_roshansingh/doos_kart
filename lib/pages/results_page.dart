import 'package:doos_kart/widget/friend_button_clipper.dart';
import 'package:doos_kart/widget/participants.dart';
import 'package:doos_kart/widget/winner.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ResultsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF1d398d),
      bottomNavigationBar: Container(
        color: Colors.transparent,
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 100),
        child: ClipPath(
            clipper: FClipper(),
            child: Material(
                color: Color(0xFF12a23c),
                child: InkWell(
                    onTap: () {},
                    child: Container(
                        height: 48,
                        width: 200,
                        alignment: Alignment.center,
                        child: Icon(
                          Icons.check,
                          color: Colors.white,
                        ))))),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
            child: Column(
              children: [
                Text(
                  'Win Them #1',
                  style: GoogleFonts.cairo().copyWith(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.w900),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Winner(
                        position: 2,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Winner(
                        position: 1,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Winner(
                        position: 3,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Column(
                        children: [
                          Text(
                            'User',
                            style: GoogleFonts.cairo().copyWith(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                          Text('00:35.32',
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w300))
                        ],
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'Sultan Akshareef',
                            style: GoogleFonts.cairo().copyWith(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                          Text('00:35.32',
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w300))
                        ],
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Column(
                        children: [
                          Text(
                            'User',
                            style: GoogleFonts.cairo().copyWith(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                          Text('00:35.32',
                              style: GoogleFonts.cairo().copyWith(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w300))
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  itemBuilder: (context, i) {
                    return Participants(i);
                  },
                  itemCount: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
