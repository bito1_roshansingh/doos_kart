import 'package:doos_kart/pages/add_friend_page.dart';
import 'package:doos_kart/widget/friend_button_clipper.dart';
import 'package:doos_kart/widget/friends.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FriendsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 20.0),
            child: ClipPath(
                clipper: FClipper(),
                child: Material(
                    color: Color(0xFF12a23c),
                    // margin: const EdgeInsets.symmetric(horizontal: 20.0),
                    // width: MediaQuery.of(context).size.width,
                    child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AddFriendPage()));
                        },
                        child: Container(
                            height: 48,
                            alignment: Alignment.center,
                            margin:
                                const EdgeInsets.symmetric(horizontal: 20.0),
                            width: MediaQuery.of(context).size.width * .7,
                            child: Text(
                              'Add Friend',
                              style: GoogleFonts.cairo().copyWith(
                                  fontSize: 19,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ))))),
          ),
          Container(
            margin: const EdgeInsets.only(left: 12.0),
            child: ListView.builder(
              primary: false,
              itemBuilder: (context, i) {
                return Friends(i);
              },
              itemCount: 7,
              shrinkWrap: true,
            ),
          ),
        ],
      ),
    );
  }
}
