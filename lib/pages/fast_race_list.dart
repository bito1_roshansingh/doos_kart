import 'package:doos_kart/widget/fast_race.dart';
import 'package:flutter/material.dart';

class FastRaceList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ListView.builder(
        primary: false,
        itemBuilder: (context, i) {
          return FastRace(i);
        },
        itemCount: 7,
        shrinkWrap: true,
      ),
    );
  }
}
